class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;

    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._name = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._name = value;
    }

}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get lang() {
        return this._lang;
      }
    
      set lang(lang) {
        this._lang = lang;
      }
      get salary() {
        return super.salary * 3;
      }
}

const programmer1 = new Programmer('Jack', 36, 5800, ['ru', 'ua']);
const programmer2 = new Programmer('Tom', 45, 4700, ['pl', 'en']);
console.log(programmer1);
console.log(programmer2);